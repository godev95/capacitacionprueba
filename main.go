package main

import (
	"fmt"
)

type Cliente struct {
	Nombre    string
	Apellido  string
	Direccion string
	Poliza    string
}

func main() {

	saludo := "Hola Mundo"

	fmt.Println(saludo)

	nombre := GetPolicy()

	fmt.Println(nombre)

	if RetornarEstado() {

		fmt.Println("verdadero")
	} else {

		fmt.Println("false")
	}

}

func RetornarEstado() bool {

	return true

}

func GetPolicy() string {
	return "Hola Victor"

}

func FuncionAnonima() interface{} {

	return func() {
		fmt.Println("Hola Mundo")
	}

}
